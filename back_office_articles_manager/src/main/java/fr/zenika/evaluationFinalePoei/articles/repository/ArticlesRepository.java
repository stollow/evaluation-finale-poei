package fr.zenika.evaluationFinalePoei.articles.repository;

import fr.zenika.evaluationFinalePoei.articles.models.Articles;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface ArticlesRepository extends CrudRepository<Articles,String> {
    @Override
    @Query(" select a from Articles a " +
            " where a.state != 0")
    Iterable<Articles> findAll();

    @Query(" select a from Articles a " +
            " where a.state != 0 OR a IN (select a from Articles JOIN User u ON a.user.id = ?1)")
    Iterable<Articles> findAllWhenAuthenticated(Integer id);
}
