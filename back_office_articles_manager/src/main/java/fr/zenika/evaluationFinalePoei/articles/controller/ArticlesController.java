package fr.zenika.evaluationFinalePoei.articles.controller;

import fr.zenika.evaluationFinalePoei.articles.auth.UserService;
import fr.zenika.evaluationFinalePoei.articles.models.Articles;
import fr.zenika.evaluationFinalePoei.articles.representation.ArticlesRepresentation;
import fr.zenika.evaluationFinalePoei.articles.services.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/articles")
public class ArticlesController {

    private ArticlesService service;
    private ArticlesRepresentation representation;

    @Autowired
    public ArticlesController(ArticlesService service,ArticlesRepresentation representation) {
        this.service = service;
        this.representation = representation;
    }

    @GetMapping
    public List<ArticlesRepresentation> getAll(){
        List<Articles> articles = this.service.getAllArticles();
        return articles.stream()
                .map(this.representation::mapArticleToRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<ArticlesRepresentation> getOneArticle(@PathVariable("id") String id) {
        Optional<Articles> article = this.service.findById(id);
        return article
                .map(this.representation::mapArticleToRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/auth/{userName}")
    public List<ArticlesRepresentation> getAllWhenAuthenticated(@PathVariable("userName") String userName){
        List<Articles> articles = this.service.getAllArticles(userName);
        return articles.stream()
                .map(this.representation::mapArticleToRepresentation)
                .collect(Collectors.toList());
    }


    @PostMapping
    public ResponseEntity<ArticlesRepresentation>
    createNewArticle(@RequestBody(required = false) ArticlesRepresentation body){
        Optional<Articles> articles = Optional.ofNullable(this.representation.mapRepresentationToArticle(body));
        return articles
                .map(article -> this.service.save(article))
                .map(this.representation::mapArticleToRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/update")
    public ResponseEntity<ArticlesRepresentation>
    createNewArticle(@RequestBody(required = false) Articles body){
        Optional<Articles> articles = Optional.ofNullable(body);
        return articles
                .map(article -> this.service.save(article))
                .map(this.representation::mapArticleToRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
