package fr.zenika.evaluationFinalePoei.articles;

import fr.zenika.evaluationFinalePoei.articles.auth.UserService;
import fr.zenika.evaluationFinalePoei.articles.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticlesApplication implements CommandLineRunner {
	@Autowired
	UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(ArticlesApplication.class, args);
	}

	@Override
	public void run(String... args) {
		this.userService.registerNewUserAccount(new User("ADMIN","ADMIN","truc@truc.fr"));
		this.userService.registerNewUserAccount(new User("Auteur Inconnu","1","anonymous@anonymous"));
	}
}
