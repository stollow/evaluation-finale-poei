package fr.zenika.evaluationFinalePoei.articles.representation;

import fr.zenika.evaluationFinalePoei.articles.auth.UserService;
import fr.zenika.evaluationFinalePoei.articles.enums.ArticleState;
import fr.zenika.evaluationFinalePoei.articles.models.Articles;
import fr.zenika.evaluationFinalePoei.articles.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
public class ArticlesRepresentation {
    private String id;
    private String title;
    private String content;
    private String resume;
    private String userName;
    private Integer state;

    public ArticlesRepresentation(){}

    @Autowired
    private UserService userService;



    public ArticlesRepresentation(String id,String title, String content, String resume,String userName,Integer state) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.resume = resume;
        this.userName = userName;
        this.state = state;
    }

    public ArticlesRepresentation mapArticleToRepresentation(Articles a){
        String u;
        if(null != a.getUser()){
            u = a.getUser().getUsername();
        }else {
            u = "Auteur Inconnu";
        }
        return new ArticlesRepresentation(a.getId(),a.getTitle(),a.getContent(),a.getResume(),u,a.getState().ordinal());
    }

    public Articles mapRepresentationToArticle(ArticlesRepresentation a){
        User u = this.userService.loadUserByUsername(a.getUserName());
        Articles article = new Articles(a.title,a.content,a.resume,u);
        article.setState(ArticleState.values()[a.getState()]);
        return article;
    }

    public String getUserName() {
        return userName;
    }

    public Integer getState() {
        return state;
    }


    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getResume() {
        return resume;
    }
}
