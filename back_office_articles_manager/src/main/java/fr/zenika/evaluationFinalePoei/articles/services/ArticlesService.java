package fr.zenika.evaluationFinalePoei.articles.services;

import fr.zenika.evaluationFinalePoei.articles.auth.UserService;
import fr.zenika.evaluationFinalePoei.articles.models.Articles;
import fr.zenika.evaluationFinalePoei.articles.repository.ArticlesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ArticlesService {

    private ArticlesRepository repository;
    private UserService userService;

    @Autowired
    public ArticlesService(ArticlesRepository repository,UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }


    public List<Articles> getAllArticles(){
        return (List<Articles>) this.repository.findAll();
    }

    public List<Articles> getAllArticles(String userName){
        Integer id = this.userService.loadUserByUsername(userName).getUserId();
        return (List<Articles>) this.repository.findAllWhenAuthenticated(id);
    }

    public Articles save(Articles a){
        return this.repository.save(a);
    }

    public Optional<Articles> findById(String id){
        return this.repository.findById(id);
    }

}
