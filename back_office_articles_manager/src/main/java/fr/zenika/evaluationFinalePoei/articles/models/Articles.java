package fr.zenika.evaluationFinalePoei.articles.models;

import fr.zenika.evaluationFinalePoei.articles.enums.ArticleState;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Articles {

    @Id
    private String id;
    private String title;
    private String content;
    private String resume;
    private ArticleState state;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="users_id")
    private User user;

    public Articles(){}

    public Articles(String title, String content, String resume,User user) {
        this.id = UUID.randomUUID().toString();
        this.title = title;
        this.content = content;
        this.resume = resume;
        this.state = ArticleState.DRAFT;
        this.user = user;
    }

    public Articles(String id, String title, String content, String resume,User user) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.resume = resume;
        this.state = ArticleState.DRAFT;
        this.user = user;
    }


    public String getId() {
        return id;
    }

    public ArticleState getState() {
        return state;
    }

    public void setState(ArticleState state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getResume() {
        return resume;
    }

}
