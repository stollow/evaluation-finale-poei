package fr.zenika.evaluationFinalePoei.articles.enums;

public enum ArticleState {
    DRAFT,
    PUBLISHED
}
