package fr.zenika.evaluationFinalePoei.articles.security;

public final class SecurityConstants {

    public static final String AUTH_LOGIN_URL = "/login";

    // Signing key for HS512 algorithm
    // You can use the page http://www.allkeysgenerator.com/ to generate all kinds of keys
    public static final String JWT_SECRET = "9rvA8KHg0CEsg4FaX8VvTc1uY6GV8HIpSobf3Q==A36rSbSBuEykEWFBFBnE7sRS";

    // JWT token defaults
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "HARDY charles";
    public static final String TOKEN_AUDIENCE = "articlesManager";

    private SecurityConstants() {
        throw new IllegalStateException("Cannot create instance of static util class");
    }
}