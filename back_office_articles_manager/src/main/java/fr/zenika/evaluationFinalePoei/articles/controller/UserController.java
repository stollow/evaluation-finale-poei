package fr.zenika.evaluationFinalePoei.articles.controller;

import fr.zenika.evaluationFinalePoei.articles.auth.UserService;
import fr.zenika.evaluationFinalePoei.articles.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;


    private Authentication getAuth(){
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @PostMapping
    public ResponseEntity<User>
    createUser(@RequestBody(required = false) User body){
        Optional<User> user = Optional.ofNullable(body);
        return user
                .map(article -> this.userService.registerNewUserAccount(user.get()))
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}