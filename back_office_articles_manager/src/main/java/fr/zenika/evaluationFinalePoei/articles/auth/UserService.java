package fr.zenika.evaluationFinalePoei.articles.auth;

import org.springframework.beans.factory.annotation.Autowired;
import fr.zenika.evaluationFinalePoei.articles.models.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        Objects.requireNonNull(username);
        User user = userRepository.findUserWithName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return user;
    }

    public User registerNewUserAccount(User u) {
        User user = new User();
        user.setUsername(u.getUsername());
        user.setEmail(u.getEmail());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(11);

        user.setPassword(passwordEncoder.encode(u.getPassword()));
        return userRepository.save(user);
    }

}
