import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Articles} from './app/models/Articles';
import {Observable} from 'rxjs';
import {User} from './app/models/User';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private http: HttpClient) { }

  public getArticles(): Observable<Articles[]> {
      return this.http.get<Articles[]>('http://localhost:8080/articles');
  }

  public getArticlesWhenAuth(userName: string): Observable<Articles[]> {
    return this.http.get<Articles[]>('http://localhost:8080/articles/auth/' + userName);
  }

  public getArticle(id: string): Observable<Articles> {
    return this.http.get<Articles>('http://localhost:8080/articles/' + id);
  }

  public createArticle(data: Articles): Observable<Articles> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'})
    };
    return this.http.post<Articles>('http://localhost:8080/articles', data, httpOptions);
  }

  public updateArticle(data: Articles): Observable<Articles> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'})
    };
    return this.http.post<Articles>('http://localhost:8080/articles/update', data, httpOptions);
  }

}
