import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ArticlesLayoutComponent} from './articles-layout/articles-layout.component';
import {LoginComponentComponent} from './login-component/login-component.component';
import {ArticleViewComponent} from './article-view/article-view.component';
import {ArticleCreationComponent} from './article-creation/article-creation.component';
import {UserCreationComponent} from './user-creation/user-creation.component';


const routes: Routes = [
  { path: 'articles', component: ArticlesLayoutComponent },
  { path: 'login',      component: LoginComponentComponent },
  {path: '403', component: LoginComponentComponent},
  {path: 'articles/:id', component: ArticleViewComponent},
  {path: 'createArticles', component: ArticleCreationComponent},
  {path: 'createUser', component: UserCreationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
