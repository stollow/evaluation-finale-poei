import {Component, Input, OnInit} from '@angular/core';
import {Articles} from "../models/Articles";
import {Router} from '@angular/router';
import {UserService} from '../../user.service';

@Component({
  selector: 'article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input() article: Articles;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  gotoDetails(article: Articles) {
    this.router.navigate(['/articles/', article.id]);
  }

}

