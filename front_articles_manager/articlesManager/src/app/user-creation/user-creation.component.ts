import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from '../../user.service';
import {User} from '../models/User';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-creation',
  templateUrl: './user-creation.component.html',
  styleUrls: ['./user-creation.component.css']
})
export class UserCreationComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.userService.createUser(new User(form.value.username, form.value.email, form.value.password)).subscribe(response => {
      console.log(response);
      this.router.navigate(['/login']);
    });
  }

}
