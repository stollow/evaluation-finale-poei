import { Component, OnInit } from '@angular/core';
import {Articles} from '../models/Articles';
import {ArticlesService} from '../../articles.service';
import { Router } from '@angular/router';
import {UserService} from '../../user.service';
import {AuthenticationService} from '../_services/authentication.service';

@Component({
  selector: 'app-articles-layout',
  templateUrl: './articles-layout.component.html',
  styleUrls: ['./articles-layout.component.css']
})
export class ArticlesLayoutComponent implements OnInit {

  private articles: Articles[];
  private isConnected = false;

  constructor(private service: ArticlesService,
              private router: Router, private userService: UserService, private authentication: AuthenticationService) {
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser') != null) {
      console.log(localStorage.getItem('currentUser'));
      this.isConnected = true;
      this.service.getArticlesWhenAuth(JSON.parse(localStorage.getItem('currentUser')).username).subscribe(
        (data) => {
          this.articles = data;
        });
    } else {
      this.service.getArticles().subscribe(
        (data) => {
          this.articles = data;
        });
    }
  }

  logout() {
    this.authentication.logout();
    this.router.navigate(['/login']);
  }
  }
