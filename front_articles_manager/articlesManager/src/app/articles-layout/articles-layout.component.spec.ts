import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesLayoutComponent } from './articles-layout.component';

describe('ArticlesLayoutComponent', () => {
  let component: ArticlesLayoutComponent;
  let fixture: ComponentFixture<ArticlesLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
