import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { ArticlesLayoutComponent } from './articles-layout/articles-layout.component';
import { ArticleComponent } from './article/article.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import {FormsModule} from '@angular/forms';
import { ArticleViewComponent } from './article-view/article-view.component';
import {JwtInterceptor} from './_helpers/jwt.interceptor';
import { ArticleCreationComponent } from './article-creation/article-creation.component';
import { UserCreationComponent } from './user-creation/user-creation.component';

@NgModule({
  declarations: [
    AppComponent,
    ArticlesLayoutComponent,
    ArticleComponent,
    LoginComponentComponent,
    ArticleViewComponent,
    ArticleCreationComponent,
    UserCreationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
