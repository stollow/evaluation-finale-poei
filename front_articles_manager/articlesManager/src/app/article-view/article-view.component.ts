import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticlesService} from '../../articles.service';
import {Articles} from '../models/Articles';
@Component({
  selector: 'app-article-view',
  templateUrl: './article-view.component.html',
  styleUrls: ['./article-view.component.css']
})
export class ArticleViewComponent implements OnInit {

  isLoading = true;
  article: Articles;
  isAuthorAuth = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ArticlesService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getArticle(id)
      .subscribe( response => {
        this.article = response;
        this.isLoading = false;
        if (JSON.parse(localStorage.getItem('currentUser')).username === this.article.userName) {
          this.isAuthorAuth = true;
        }
      });
  }

  publishArticle() {
    this.article.state = 1;
    this.service.updateArticle(this.article).subscribe(response => {
      console.log(response);
    });
  }

}
