import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {ArticlesService} from '../../articles.service';
import {UserService} from '../../user.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../_services/authentication.service';
import {map} from 'rxjs/operators';
import {User} from '../models/User';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {

  error = false;

  constructor(private service: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.service.login(form.value.username, form.value.password).subscribe(
      data => {
        const user: User = JSON.parse(localStorage.getItem('currentUser'));
        user.token = data.headers.get('Authorization');
        localStorage.setItem('currentUser', JSON.stringify(user));
        console.log(localStorage.getItem('currentUser'));
        this.router.navigate(['/articles']);
    },
      (error) => {
        this.service.logout();
        this.error = true;
      });
  }

}
