import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ArticlesService} from '../../articles.service';
import {Articles} from '../models/Articles';
import {Router} from '@angular/router';

@Component({
  selector: 'app-article-creation',
  templateUrl: './article-creation.component.html',
  styleUrls: ['./article-creation.component.css']
})
export class ArticleCreationComponent implements OnInit {

  constructor(private service: ArticlesService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form.value.title);
    const article = new Articles(form.value.title, form.value.short,
      form.value.article, JSON.parse(localStorage.getItem('currentUser')).username, 0);
    this.service.createArticle(article).subscribe(response => {
      console.log(response);
      this.router.navigate(['/articles']);
    });
  }

}
