export class User {
  email?: string;
  username: string;
  userId?: number;
  token?: string;
  password?: string;

  constructor(username: string, email?: string, password?: string) {
    this.username = username;
    this.email = email;
    this.password = password;
  }

}
