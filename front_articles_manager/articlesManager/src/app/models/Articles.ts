import {User} from './User';

export class Articles {
  content: string;
  resume: string;
  id: string;
  title: string;
  userName: string;
  state?: number;

  constructor(title: string, resume: string, content: string,  userName: string, state?: number) {
    this.content = content;
    this.resume = resume;
    this.title = title;
    this.userName = userName;
    this.state = state;
  }
}
